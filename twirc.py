# -*- coding: future_fstrings -*-
from __future__ import unicode_literals

SCRIPT_NAME = 'twirc'
SCRIPT_AUTHOR = 'z59 <z59@protonmail.com>'
SCRIPT_VERSION = '1'
SCRIPT_LICENSE = 'GPL3'
SCRIPT_DESC = 'Twitter bot for WeeChat'

import weechat, json, re, urllib
from HTMLParser import HTMLParser  # import html in python3
html = HTMLParser()

settings = {
  'buffer': 'ua #ua-news',
  'update_period': '60',
  'authorization': '',
  'xcsrftoken': '',
  'cookie': '',
  'cursor': ''
}
x2s = r'(?:[\n\s]*https://[^\s]*[\n\s]*)+|[\n\s]+'
urlbuf = ''

def url_cb(data, command, return_code, out, err):
  global urlbuf
  if out != '':
    urlbuf += out
  if err != '':
    weechat.prnt('', f'{SCRIPT_NAME}: url process: {err}')
  if return_code == weechat.WEECHAT_HOOK_PROCESS_ERROR:
    urlbuf = ''
  elif int(return_code) >= 0:
    buffer = weechat.config_get_plugin('buffer')
    home = json.loads(urlbuf)
    urlbuf = ''
    hidden = set()
    objs = home['globalObjects']
    tw_dict = objs['tweets']
    for instr in home['timeline']['instructions']:
      for entry in instr.get('addEntries', {}).get('entries', []):
        etyp = entry['entryId'].split('-')  # example: cursor-top-1234
        etyp0 = etyp[0]
        if etyp0 == 'promotedTweet':
          hidden.add(entry['content']['item']['content']['tweet']['id'])  # hide ads
        elif etyp0 == 'tweet':
          etw = entry['content']['item']['content']['tweet']
          text = etw.get('socialContext', {}).get('generalContext', {}).get('text')
          if text != None:  # if {user} liked {tweet}
            tw = tw_dict[ etw['id'] ]
            tw['full_text'] = f'({text}) ' + tw['full_text']
        elif etyp0 == 'cursor' and etyp[1] == 'top':
          cursor = urllib.quote_plus( entry['content']['operation']['cursor']['value'] )  # urllib.parse.quote_plus in python3
    for tid, tw in tw_dict.iteritems():  # .items() in python3
      if tw.get('entities', {}).get('urls') != None:
        tw['full_text'] += ' [LNK]'
      for media in tw.get('extended_entities', {}).get('media', []):
        media_t = media['type']
        media_t = {
          'photo': 'PIC',
          'video': 'VIDEO',
          'animated_gif': 'GIF',
        }.get(media_t, media_t)
        tw['full_text'] += f' [{media_t}]'
    def handle_rt(tid, tw):
      rtid = is_rt = tw.pop('retweeted_status_id_str', None)
      if rtid == None:
        rtid = tw.pop('quoted_status_id_str', None)
        if rtid == None:
          return
      rtw = tw_dict.get(rtid)
      if rtw != None:  # check if retweet data included in twitter response
        handle_rt(rtid, rtw)
        runame = objs['users'][ rtw['user_id_str'] ]['screen_name']
        rtext = rtw['full_text']
        if is_rt != None:  # rt = not quoted
          tw['full_text'] = ''
        tw['full_text'] += f' RT @{runame}: {rtext}'
        hidden.add(rtid)  # hide retweeted
    for tid, tw in tw_dict.iteritems():  # .items() in python3
      handle_rt(tid, tw)
    for tid, tw in tw_dict.iteritems():  # .items() in python3
      if tid not in hidden:
        uname = objs['users'][ tw['user_id_str'] ]['screen_name']
        text = re.sub(x2s, ' ', html.unescape(tw['full_text'])).strip()
        weechat.command('', f'/msg -server {buffer} https://twitter.com/{uname}/status/{tid} {text}'.encode('utf-8'))
    weechat.config_set_plugin('cursor', cursor)
  return weechat.WEECHAT_RC_OK

def timer_cb(data, remaining_calls):
  authorization = weechat.config_get_plugin('authorization')
  xcsrftoken = weechat.config_get_plugin('xcsrftoken')
  cookie = weechat.config_get_plugin('cookie')
  cursor = weechat.config_get_plugin('cursor')
  if authorization == '' or xcsrftoken == '' or cookie == '' or cursor == '':
    return weechat.WEECHAT_RC_OK
  url = f'https://twitter.com/i/api/2/timeline/home.json?include_profile_interstitial_type=1&include_blocking=1&include_blocked_by=1&include_followed_by=1&include_want_retweets=1&include_mute_edge=1&include_can_dm=1&include_can_media_tag=1&skip_status=1&cards_platform=Web-12&include_cards=1&include_ext_alt_text=true&include_quote_count=true&include_reply_count=1&tweet_mode=extended&include_entities=true&include_user_entities=true&include_ext_media_color=true&include_ext_media_availability=true&send_error_codes=true&simple_quoted_tweet=true&earned=1&cursor={cursor}&lca=true&ext=mediaStats%2ChighlightedLabel'
  options = {
    'httpheader': '\n'.join([
      'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0',
      f'authorization: {authorization}',  # constant for user
      f'x-csrf-token: {xcsrftoken}',  # variable
      f'Cookie: {cookie}'
    ])
  }
  weechat.hook_process_hashtable('url:' + url, options, 30 * 1000, 'url_cb', '')
  return weechat.WEECHAT_RC_OK

if __name__ == '__main__' and \
weechat.register(SCRIPT_NAME, SCRIPT_AUTHOR, SCRIPT_VERSION, SCRIPT_LICENSE, SCRIPT_DESC, '', ''):
  for option, default_value in settings.iteritems():  # .items() in python3
    if not weechat.config_is_set_plugin(option):
      weechat.config_set_plugin(option, default_value)
  weechat.hook_timer(int(weechat.config_get_plugin('update_period')) * 1000, 0, 0, 'timer_cb', '')
